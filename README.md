### Codavel SDK ###

The purpose of this project is to demonstrate how easy is to integrate Codavel SDK into an Android app, by modifying wikipedia-ios, the official Wikipedia iOS app.

You can check the original README [here](README.original.md), or directly in the original repository [here](https://github.com/wikimedia/wikipedia-ios).


### Integration ###

In order to integrate Codavel SDK, the following modifications to the original project were required (you can check all the code changes [here](https://gitlab.com/codavel/wikipedia-ios/-/compare/main...codavel?from_project_id=29762473)):

1. Include Codavel's dependency into the applications' Podfile, required to download and use Codavel's SDK
2. Start Codavel's Service with Codavel's Application ID and Secret when the app starts, by changing the didFinishLaunchingWithOptions method in the AppDelegate.
3. Register our HTTP interceptor into the application's URLSessionConfiguration, so that all the HTTP requests executed through the app's are processed and forwarded to our SDK.
